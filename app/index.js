// show mobile form
$('.js-show-form').click(function(){
    $('#mobile-info').hide();
    $('#mobile-form').show();
});

// hide mobile form
$('#mobile-cancel, #mobile-save').click(function() {
    $('#mobile-form').hide();
    $('#mobile-info').show();
});

// show desktop form
$('#edit-name').click(function() {
	$('#form-name').show();
});

$('.js-cancel').click(function() {
	$('#form-name, #form-website, #form-phone, #form-address').hide();
});

$('.js-save').click(function() {
	$('#form-name, #form-website, #form-phone, #form-address').hide();
});

$('#edit-website').click(function() {
	$('#form-website').show();
});

$('#edit-phone').click(function() {
	$('#form-phone').show();
});

$('#edit-address').click(function() {
	$('#form-address').show();
});